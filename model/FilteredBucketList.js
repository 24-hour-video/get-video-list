const AWS = require('aws-sdk')
const s3 = new AWS.S3()

class FilteredBucketList {
  /**
   *
   * @param bucket
   * @param extension string without the dot
   */
  constructor (bucket, baseURL, extension = 'jpg') {
    this.bucket = bucket
    this.baseURL = baseURL
    this.extension = extension
  }

  list() {
    const listParams = {
      Bucket: this.bucket,
      EncodingType: 'url'
    }

    return s3.listObjects(listParams).promise()
      .then(results => {
        console.log("results")
        const urls =  results.Contents.filter(file => file.Key && file.Key.substr(-3, 3) === this.extension)
        return {
          baseUrl: this.baseURL,
          bucket: this.bucket,
          urls
        }
      })
      .catch(err => {
        console.log("error", err)
        throw new Error('Bucket list error')
      })
  }
}

module.exports = FilteredBucketList;