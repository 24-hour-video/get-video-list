'use strict';
const FilteredBucketList = require('./model/FilteredBucketList')
const extension = process.env.EXTENSION || 'png'
const headers = [
  { 'Access-Control-Allow-Headers': 'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token' },
  { 'Access-Control-Allow-Methods': 'GET,OPTIONS' },
  { 'Access-Control-Allow-Origin:': '*' },
]

exports.handler = async () => {
  try  {
    const list = await (new FilteredBucketList(process.env.BUCKET, process.env.BASE_URL, extension)).list()
    return {
      statusCode: 200,
      headers,
      body: JSON.stringify(list)
    }
  } catch(err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify(err)
    }
  }
}
