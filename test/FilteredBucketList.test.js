const chai = require('chai')
const sinon = require('sinon')
const rewire = require('rewire')
const expect = chai.expect

const mockedBucketListResponse = (type='jpg') => {
  const mockedBucketListResponse = {
    Contents: [
      {
        ETag: '"70ee1738b6b21e2c8a43f3a5ab0eee71"',
        Key: 'example1.'+type,
        LastModified: '2018-12-05',
        Owner: {
          DisplayName: 'myname',
          ID: '12345example25102679df27bb0ae12b3f85be6f290b936c4393484be31bebcc'
        },
        Size: 11,
        StorageClass: 'STANDARD'
      },
      {
        ETag: '"70ee1738b6b21e2c8a43f3a5ab0eee712"',
        Key: 'example2.'+type,
        LastModified: '2018-12-04',
        Owner: {
          DisplayName: 'myname',
          ID: '12345example25102679df27bb0ae12b3f85be6f290b936c4393484be31bebc2'
        },
        Size: 14,
        StorageClass: 'STANDARD'
      },
    ],
    NextMarker: 'eyJNYXJrZXIiOiBudWxsLCAiYm90b190cnVuY2F0ZV9hbW91bnQiOiAyfQ=='
  }
  return mockedBucketListResponse
}

describe('index.js lists a bucket', () => {
  describe('when there are two files in the bucket with jpg', () => {
    it('should list the two files with jpg extension', async () => {
      // mock s3 dependency to return two jpg files
      const MockedFilteredBucketList = rewire('../model/FilteredBucketList')

      // we have to mock this call: s3.listObjects(params).promise() to be a promise with the result
      MockedFilteredBucketList.__set__({
        's3': {
          listObjects: (params) => {
            return {
              promise: () => {
                return Promise.resolve(mockedBucketListResponse())
              }
            }
          }
        }
      })

      const listmaker = new MockedFilteredBucketList('bucketname', 'https://s3.aws.com')
      const list = await listmaker.list()
      expect(list.urls.length).to.equal(2)
      expect(list.urls[0].Key).to.equal('example1.jpg')
      expect(list.urls[1].Key).to.equal('example2.jpg')
    })

    it('should list the two files with png extension', async () => {
      // mock s3 dependency to return two jpg files
      const MockedFilteredBucketList = rewire('../model/FilteredBucketList')

      // we have to mock this call: s3.listObjects(params).promise() to be a promise with the result
      MockedFilteredBucketList.__set__({
        's3': {
          listObjects: (params) => {
            return {
              promise: () => {
                return Promise.resolve(mockedBucketListResponse('png'))
              }
            }
          }
        }
      })

      const listmaker = new MockedFilteredBucketList('bucketname', 'https://s3.aws.com')
      const list = await listmaker.list()
      expect(list.urls.length).to.equal(0)
    })
  })
})

